package example;

import java.util.List;

import org.generico.exception.DAOException;
import org.springframework.stereotype.Component;

/**
 * @name
 * ExampleServiceImpl
 * 
 * @description
 * Basic service implementation for test demonstration purposes.
 * 
 * @author Alexander Kiennast
 *
 */
@Component
public class ExampleServiceImpl implements ExampleService{
	
	private ExampleDao exampleDAO=new ExampleDaoImpl();

	/**
	 * Returns a list of all persisted entities.
	 * Uses the getEntityListByDate() method of the ExampleDAO.
	 * @return List<ExampleEntity> - all persisted entities
	 */
	public List<ExampleEntity> getAllEntities() {
		return exampleDAO.getEntityListByDate();
	}
	
	/**
	 * Persists the specified entity.
	 * Uses the createEntity() method of the ExampleDAO.
	 * @param entity - the entity to persist.
	 * @throws DAOException
	 */
	public void persistEntity(ExampleEntity entity) throws DAOException{
		exampleDAO.createEntity(entity);
	}

	/**
	 * Reads all persisted entities from the database.
	 * Uses the getEntityListByDate() method of the ExampleDAO.
	 * Then calculates a total sum of the entitie's number attributes.
	 * @return int - sum of the entitie's number attributes
	 */
	public int calculateNumberSum() {
		List<ExampleEntity> list=exampleDAO.getEntityListByDate();
		int i=0;
		int sum=0;
		if(list!=null){
			while(i<list.size()){
				sum=sum+list.get(i).getNumber();
			}
		}
		return sum;
	}

	
	public ExampleDao getExampleDAO() {
		return exampleDAO;
	}

	public void setExampleDAO(ExampleDao exampleDAO) {
		this.exampleDAO = exampleDAO;
	}
}
