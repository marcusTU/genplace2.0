package example;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.generico.domain.BasicEntity;

/**
 * @name
 * ExampleEntity
 * 
 * @description
 * Basic entity for test demonstration purposes.
 * 
 * @author Alexander Kiennast
 *
 */
@Entity
public class ExampleEntity extends BasicEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2055321112137819693L;
	
	/**
	 * A simple String attribute.
	 */
	@NotNull
	private String name;
	
	/**
	 * A simple int attribute.
	 */
	@NotNull
	private int number;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
}
