package example;

import java.util.List;

import org.generico.persistance.SimpleDao;

/**
 * @name
 * ExampleDAO
 * 
 * @description
 * Basic DAO interface for test demonstration purposes.
 * 
 * @author Alexander Kiennast
 *
 */
public interface ExampleDao extends SimpleDao<ExampleEntity>{
	
	/**
	 * Returns a list of all persisted entities, ordered by date of creation.
	 * @return List<ExampleEntity> - all entities, ordered by date
	 */
	public List<ExampleEntity> getEntityListByDate();
	
}
