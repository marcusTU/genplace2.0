package example;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.generico.persistance.BasicDao;
import org.springframework.stereotype.Component;

/**
 * @name
 * ExampleDAOImpl
 * 
 * @description
 * Basic DAO implementation for test demonstration purposes.
 * 
 * @author Alexander Kiennast
 *
 */
@Component
public class ExampleDaoImpl extends BasicDao<ExampleEntity> implements ExampleDao{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7812806997730264295L;
	
	/**
	 * Returns this class.
	 */
	@Override
	protected Class<ExampleEntity> getClazz() {
		return ExampleEntity.class;
	}
	
	/**
	 * Returns a list of all persisted entities, ordered by date of creation.
	 * @return List<ExampleEntity> - all entities, ordered by date
	 */
	public List<ExampleEntity> getEntityListByDate() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ExampleEntity> criteria = cb.createQuery(ExampleEntity.class);
		Root<ExampleEntity> ExampleEntity = criteria.from(ExampleEntity.class);

		criteria.select(ExampleEntity).orderBy(cb.desc(ExampleEntity.get("creationDate")));
		return em.createQuery(criteria).getResultList();
	}
	
}
