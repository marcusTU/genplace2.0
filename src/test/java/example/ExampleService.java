package example;

import java.util.List;

import org.generico.exception.DAOException;

/**
 * @name
 * ExampleService
 * 
 * @description
 * Basic service interface for test demonstration purposes.
 * 
 * @author Alexander Kiennast
 *
 */
public interface ExampleService {
	
	/**
	 * Returns a list of all persisted entities.
	 * @return List<ExampleEntity> - all persisted entities
	 */
	public List<ExampleEntity> getAllEntities();
	
	/**
	 * Persists the specified entity.
	 * @param entity - the entity to persist.
	 * @throws DAOException
	 */
	public void persistEntity(ExampleEntity entity) throws DAOException;
	
	/**
	 * Reads all persisted entities from the database.
	 * Then calculates a total sum of the entitie's number attributes.
	 * @return int - sum of the entitie's number attributes
	 */
	public int calculateNumberSum();
	
}
