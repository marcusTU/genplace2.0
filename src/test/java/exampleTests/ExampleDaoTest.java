package exampleTests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.transaction.annotation.Transactional;

import example.ExampleDaoImpl;
import example.ExampleEntity;

/**
 * @name
 * ExampleDaoTest
 * 
 * @description
 * Test class for the ExampleDao.
 * Serves as template for all persistence layer unit tests.
 * 
 * Transactional - annotation provides automatic database rollback after each test is executed
 * ContextConfiguration - annotation makes sure the correct test enviroment and database is selected
 * 
 * @author Alexander Kiennast
 *
 */
@Transactional
@ContextConfiguration("file:src/test/resources/applicationContext.xml")
public class ExampleDaoTest extends AbstractJUnit4SpringContextTests{
	
	/**
	 * The Dao that is being tested.
	 */
	private ExampleDaoImpl exampleDao;
	
	/**
	 * Before annotation used instead of a constructor.
	 * Instantiates the tested service class.
	 */
	@Before
	public void construct(){
		exampleDao=new ExampleDaoImpl();
	}
	
	/**
	 * Checks if three objects inserted into the database are 
	 * returned in the correct order by the getEntityListByDate method.
	 * 
	 * Because of the Transactional- annotation of the test class,
	 * all database changes are automatically rolled back at the method end.
	 */
	@Test
	public void testGetEntityListByDateOrder(){
		
		ExampleEntity testEntity1=new ExampleEntity();
		testEntity1.setName("One");
		testEntity1.setNumber(1);
		
		ExampleEntity testEntity2=new ExampleEntity();
		testEntity2.setName("Two");
		testEntity2.setNumber(2);
		
		ExampleEntity testEntity3=new ExampleEntity();
		testEntity3.setName("Three");
		testEntity3.setNumber(3);
		
		try{
			exampleDao.createEntity(testEntity1);
			exampleDao.createEntity(testEntity2);
			exampleDao.createEntity(testEntity3);
			
			List<ExampleEntity> queryOutput=exampleDao.getEntityListByDate();
			
			assertEquals(queryOutput.get(0).getName(), testEntity1.getName());
			assertEquals(queryOutput.get(1).getName(), testEntity2.getName());
			assertEquals(queryOutput.get(2).getName(), testEntity3.getName());
		}
		catch(Exception e){
			assert(false);
		}
	}
	
	/**
	 * Checks if the getEntityListByDate method returns a list of 
	 * the correct size.
	 * 
	 * Because of the Transactional- annotation of the test class,
	 * all database changes are automatically rolled back at the method end.
	 */
	@Test
	public void testGetEntityListByDateSize(){
		
		ExampleEntity testEntity1=new ExampleEntity();
		testEntity1.setName("One");
		testEntity1.setNumber(1);
		
		ExampleEntity testEntity2=new ExampleEntity();
		testEntity2.setName("Two");
		testEntity2.setNumber(2);
		
		ExampleEntity testEntity3=new ExampleEntity();
		testEntity3.setName("Three");
		testEntity3.setNumber(3);
		
		try{
			exampleDao.createEntity(testEntity1);
			exampleDao.createEntity(testEntity2);
			exampleDao.createEntity(testEntity3);
			
			List<ExampleEntity> queryOutput=exampleDao.getEntityListByDate();
			
			assertEquals(queryOutput.size(), 3);
		}
		catch(Exception e){
			assert(false);
		}
	}
}
