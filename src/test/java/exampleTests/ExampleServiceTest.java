package exampleTests;

import static org.junit.Assert.*;

import org.generico.exception.DAOException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

import example.ExampleDaoImpl;
import example.ExampleEntity;
import example.ExampleServiceImpl;

/**
 * @name
 * ExampleServiceTest
 * 
 * @description
 * Test class for the ExampleService.
 * Serves as template for all service layer unit tests.
 * 
 * RunWith - annotation makes the Mockito framework available in this class.
 * 
 * @author Alexander Kiennast
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ExampleServiceTest extends AbstractJUnit4SpringContextTests{
	
	/**
	 * This is the service class that is to be tested.
	 * Here a real instance is used of course.
	 */
	private ExampleServiceImpl exampleService;
	
	/**
	 * This is the DAO class, which the tested service needs to call.
	 * It is mocked via Mockito, which simulates expected, correct behaivour of this class.
	 */
	@Mock
	private ExampleDaoImpl exampleDAO;
	
	/**
	 * This is the entity class, which the tested service needs to access.
	 * It is mocked via Mockito, which simulates expected, correct behaivour of this class.
	 */
	@Mock
	private ExampleEntity exampleEntity;
	
	/**
	 * Before annotation used instead of a constructor.
	 * Instantiates the tested service class.
	 */
	@Before
	public void construct(){
		exampleService=new ExampleServiceImpl();
		exampleService.setExampleDAO(exampleDAO);
	}
	
	/**
	 * Checks if the tested service calls the DAOs getEntityListByDate() method,
	 * when its own getAllEntities() method is called.
	 */
	@Test
	public void testGetAllEntitiesCall(){
		exampleService.getAllEntities();
		Mockito.verify(exampleDAO, Mockito.times(1)).getEntityListByDate();
	}
	
	/**
	 * Checks if the tested service calls the DAOs createEntity() method,
	 * when its own persistEntity() method is called.
	 */
	@Test
	public void testPersistEntityCall() throws DAOException{
		exampleService.persistEntity(exampleEntity);
		Mockito.verify(exampleDAO, Mockito.times(1)).createEntity(exampleEntity);
	}
	
	/**
	 * Checks if the tested service calls the DAOs getEntityListByDate() method,
	 * when its own exampleService() method is called.
	 */
	@Test
	public void testGetAllEntitiesForSumCall(){
		exampleService.calculateNumberSum();
		Mockito.verify(exampleDAO, Mockito.times(1)).getEntityListByDate();
	}
	
	/**
	 * Checks if the tested service returns the sum 0,
	 * when its calculateNumberSum() is called and there are no entries in the database.
	 */
	@Test
	public void testCalculateSumNullInput(){
		assertEquals(exampleService.calculateNumberSum(), 0);
	}
	
//	@Test
//	public void testCalculateSumValue(){
//		
//	}
}
