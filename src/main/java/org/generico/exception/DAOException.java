package org.generico.exception;

/**
 * An Exception indicating an error in the DAO-Layer.
 * 
 * @author Stefan Zaufl
 * @version 1
 * @date 27.4.2013
 *
 */
public class DAOException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3559528993505231413L;

	public DAOException() {
		super();
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	public DAOException(String message) {
		super(message);
	}

	public DAOException(Throwable cause) {
		super(cause);
	}

}
