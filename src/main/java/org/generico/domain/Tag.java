package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;


/**
 * @name TagCategory
 * 
 * @description
 * Domain Object for a category of tags that can be assigned to templates.
 * 
 * @author Alexander Kiennast
 *
 */
@Entity
public class Tag extends BasicEntity implements Serializable{

	private static final long serialVersionUID = -2821204177237867284L;
	
	@NotNull
	@Field(index=Index.YES, analyze=Analyze.YES, store=Store.NO)
	@Analyzer(definition = "productAnalyzer")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
