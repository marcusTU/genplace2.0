package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * @name TemplateValueDouble
 * 
 * @description
 * Domain Object for abstract TemplateValueDouble
 * 
 * @author Marcus Presich
 *
 */

@Entity
@DiscriminatorValue(value = "3")
public class TemplateValueDouble extends TemplateValue  implements Serializable {

	private static final long serialVersionUID = -1781143192427456257L;

	@NotNull
	@Column(name="double_Value")
	private Double value;

	
	public Double getValue() {
		return value;
	}

	public void setValue(Double doubleValue) {
		this.value = doubleValue;
	}

}
