package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @name TemplateValueString
 * 
 * @description
 * Domain Object for abstract TemplateValueString
 * 
 * @author Marcus Presich
 *
 */

@Entity
@DiscriminatorValue(value = "1") 
public class TemplateValueString extends TemplateValue  implements Serializable {

	private static final long serialVersionUID = -1781143192427456257L;

	@NotNull
	@Size(min=0, max=32)
	@Column(name="String_value")
	private String value;


	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}	
}
