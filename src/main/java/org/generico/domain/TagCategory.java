package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;


/**
 * @name TagCategory
 * 
 * @description
 * Domain Object for a category of tags that can be assigned to templates.
 * 
 * @author Alexander Kiennast
 *
 */
@Entity
public class TagCategory extends BasicEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2821204177237867284L;
	
	@NotNull
	private String name;
	
	@NotNull
	private String type = "String";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
