package org.generico.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @name TemplateValueDate
 * 
 * @description
 * Domain Object for abstract TemplateValueDate
 * 
 * @author Marcus Presich
 *
 */

@Entity
@DiscriminatorValue(value = "4")
public class TemplateValueDate extends TemplateValue implements Serializable {

	private static final long serialVersionUID = -1781143192427456257L;

	@NotNull
	@Column(name="Timestamp_value")
	@Temporal(TemporalType.TIMESTAMP)
	private Date value;

	
	public Date getValue() {
		return value;
	}

	public void setValue(Date value) {
		this.value = value;
	}

}
