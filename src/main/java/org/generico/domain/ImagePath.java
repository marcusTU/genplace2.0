package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * @Name ImagePath
 * 
 * @Description
 * Domain Object for BasicProduct
 * 
 * @author Marcus Presich
 */

@Entity
public class ImagePath extends BasicEntity implements Serializable{

	/**
	 * UUID
	 */
	private static final long serialVersionUID = -4200205009809568389L;

	@NotNull
	private String imagePath;
	
	public ImagePath() {
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

}
