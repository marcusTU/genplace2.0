package org.generico.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.SnowballPorterFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

/**
 * @name BasicProduct
 * 
 * @description Domain Object for basic properties of a product
 * 
 * @author Marcus Presich
 * 
 */

@Entity
@Indexed
@AnalyzerDef(name = "productAnalyzer", tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class), filters = {
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = SnowballPorterFilterFactory.class, params = { @Parameter(name = "language", value = "German") }) })
public class BasicProduct extends BasicEntity implements Serializable {

	private static final long serialVersionUID = -1781143192427456257L;

	@NotNull
	@Field(index = Index.YES, analyze = Analyze.NO, store = Store.YES)
	@Analyzer(definition = "productAnalyzer")
	private String title;

	@NotNull
	@Field(index = Index.YES, analyze = Analyze.YES, store = Store.YES)
	@Analyzer(definition = "productAnalyzer")
	private String description;

	private Date changedDate = new Date();

	@NotNull
	private int price;

	@ManyToMany
	@IndexedEmbedded
	private List<Tag> tags = new ArrayList<Tag>();

	/**
	 * Foreign Key to template value map
	 */
	@OneToMany(targetEntity = TemplateValueMap.class)
	private List<TemplateValueMap> tempMapList = new ArrayList<TemplateValueMap>();

	/**
	 * saves the image from the basic product
	 */
	@OneToMany
	@Column(name = "productImagePaths")
	private List<ImagePath> imagePaths = new ArrayList<ImagePath>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(Date changedDate) {
		this.changedDate = changedDate;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public List<TemplateValueMap> getTempMapList() {
		return tempMapList;
	}

	public void setTempMapList(List<TemplateValueMap> tempMapList) {
		this.tempMapList = tempMapList;
	}

	/**
	 * adds our productImage to the List
	 * 
	 * @param productImage
	 */
	public void addImagePath(ImagePath productImage) {
		this.imagePaths.add(productImage);
	}

	@Override
	public String toString() {
		return "BasicProduct [title=" + title + ", description=" + description
				+ ", changedDate=" + changedDate + ", price=" + price
				+ ", tags=" + tags + ", tempMapList=" + tempMapList
				+ ", imagePaths=" + imagePaths + "]";
	}

}
