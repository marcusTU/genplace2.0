package org.generico.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * @name Template
 * 
 * @description
 * Domain Object for abstract template
 * 
 * @author Marcus Presich
 *
 */

@Entity
public class Template extends BasicEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotNull
	private String name;

	@OneToMany(targetEntity=TemplateItems.class, cascade=CascadeType.ALL)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<TemplateItems> tempItemList = new ArrayList<TemplateItems>();
	
	@ManyToMany
	private List<TagCategory> tagCategories;
	
	@Transient
	public void addTemplateItem(TemplateItems item) {
		tempItemList.add(item);
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public List<TemplateItems> getTempItemList() {
		return tempItemList;
	}


	public void setTempItemList(List<TemplateItems> tempItemList) {
		this.tempItemList = tempItemList;
	}


	public List<TagCategory> getTagCategories() {
		return tagCategories;
	}


	public void setTagCategories(List<TagCategory> tagCategories) {
		this.tagCategories = tagCategories;
	}
	
}
