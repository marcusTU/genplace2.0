package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * @name TemplateValueInt
 * 
 * @description
 * Domain Object for abstract TemplateValueInt
 * 
 * @author Marcus Presich
 *
 */

@Entity
@DiscriminatorValue(value = "2")
public class TemplateValueInt extends TemplateValue implements Serializable {

	private static final long serialVersionUID = -1781143192427456257L;

	@NotNull
	@Column(name="int_Value")
	private Integer value;

	
	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

}
