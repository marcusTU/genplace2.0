package org.generico.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @name TemplateValueMap
 * 
 * @description
 * Domain Object for abstract TemplateValueMap
 * 
 * @author Marcus Presich
 *
 */

@Entity
public class TemplateValueMap extends BasicEntity implements Serializable {

	private static final long serialVersionUID = 5739988809688312590L;
	
	@OneToOne(cascade=CascadeType.ALL)
	private TemplateValue tempValue;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private BasicProduct basicProduct;

	@ManyToOne(cascade=CascadeType.MERGE)
	private TemplateItems tempItem;
	

	public TemplateValueMap() {
		super();
	}

	public TemplateValue getTempValue() {
		return tempValue;
	}


	public void setTempValue(TemplateValue tempValue) {
		this.tempValue = tempValue;
	}


	public BasicProduct getBasicProduct() {
		return basicProduct;
	}


	public void setBasicProduct(BasicProduct basicProduct) {
		this.basicProduct = basicProduct;
	}


	public TemplateItems getTempItem() {
		return tempItem;
	}


	public void setTempItem(TemplateItems tempItem) {
		this.tempItem = tempItem;
	}

	
}
