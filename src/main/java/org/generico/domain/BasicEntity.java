package org.generico.domain;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @name BaseEntity
 * 
 * @description
 * Abstract entity class containing basic attributes that are common to all entities.
 * The other entities are expected to extend this class.
 * 
 * @author Alexander Kiennast
 *
 */
@MappedSuperclass
public abstract class BasicEntity{

	@Id
	@GeneratedValue
	protected long id;
	
	@NotNull
	protected boolean hidden = false;
	
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	protected Date creationDate = new Date();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
}
