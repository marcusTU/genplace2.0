package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * @name TemplateItems
 * 
 * @description
 * Domain Object for abstract TemplateItems
 * 
 * @author Marcus Presich
 *
 */

@Entity
public class TemplateItems extends BasicEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@NotNull
	private String label;

	@NotNull
	private int valueType;

	@NotNull
	private int displayType;
	
	@Transient
	private TemplateValue templateValue;



	public TemplateItems() {
		super();
	}
	

	public String getLabel() {
		return label;
	}



	public void setLabel(String label) {
		this.label = label;
	}



	public int getValueType() {
		return valueType;
	}



	public void setValueType(int valueType) {
		this.valueType = valueType;
	}



	public int getDisplayType() {
		return displayType;
	}



	public void setDisplayType(int displayType) {
		this.displayType = displayType;
	}



	public TemplateValue getTemplateValue() {
		return templateValue;
	}



	public void setTemplateValue(TemplateValue templateValue) {
		this.templateValue = templateValue;
	}
	
}
