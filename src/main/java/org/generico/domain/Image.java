package org.generico.domain;

import java.io.Serializable;

import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Name Image
 *
 * @Description
 * Domain Class for Image
 *
 * @author Marcus Presich
 */

public class Image  implements Serializable {

	@Transient
	private static final long serialVersionUID = -8597168328890385448L;
	
	@NotNull
	@Size(min=1, max=1024)
	private String image;

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}	
	
}
