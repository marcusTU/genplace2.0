package org.generico.domain;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.NotNull;

/**
 * @name TemplateValue
 * 
 * @description
 * Domain Object for abstract TemplateValue
 * 
 * @author Marcus Presich
 *
 */

@Entity
@DiscriminatorColumn(name="valueType", discriminatorType=DiscriminatorType.INTEGER)
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class TemplateValue extends BasicEntity implements Serializable {

	private static final long serialVersionUID = 6913986389748362292L;
	
	@NotNull	
	private int valueType;

	public int getValueType() {
		return valueType;
	}

	public void setValueType(int valueType) {
		this.valueType = valueType;
	}
	
	
}
