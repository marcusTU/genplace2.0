package org.generico.persistance;

import java.util.ArrayList;
import java.util.List;

import org.generico.domain.TagCategory;
import org.generico.domain.Template;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

/**
 * @name TemplateDAOImpl
 * 
 * @description
 * Implementation of the TemplateDAO interface.
 * 
 * 
 * @author Alexander Kiennast
 *
 */
@Component
public class TemplateDaoImpl extends BasicDao<Template> implements TemplateDao{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4545218616231042430L;
	
	private static final Logger log = Logger.getLogger(TemplateDaoImpl.class);
	
	public TemplateDaoImpl() {
		super();
	}
	
	/**
	 * returns the Template class
	 * @return class - Template
	 */
	@Override
	protected Class<Template> getClazz() {
		return Template.class;
	}

	/**
	 * Returns a list of tag categories associated with a template.
	 * @return List<TemplateCategory> - associated tag categories.
	 */
	public List<TagCategory> getAssociatedTagCategories(Template template) {
		log.info("enter getAssociatedTemplateCategoryNames...");
		
		//TODO, currently only test dummies, insert correct query
		TagCategory test1=new TagCategory();
		test1.setName("Marke");
		TagCategory test2=new TagCategory();
		test2.setName("Material");
		TagCategory test3=new TagCategory();
		test3.setName("Farbe");
		ArrayList<TagCategory> testList=new ArrayList<TagCategory>();
		testList.add(test1);
		testList.add(test2);
		testList.add(test3);
		return testList;
	}
	
}
