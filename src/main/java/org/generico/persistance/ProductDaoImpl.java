package org.generico.persistance;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.generico.domain.BasicProduct;
import org.jboss.logging.Logger;
import org.springframework.stereotype.Component;

/**
 * @name ProductDAOImpl
 * 
 * @description
 * ProductDAOImpl for the Product Domain
 * 
 * @author marcus
 *
 */

@Component
public class ProductDaoImpl extends BasicDao<BasicProduct> implements ProductDao {


	private static final long serialVersionUID = 3988925248936170724L;

	private static final Logger log = Logger.getLogger(ProductDaoImpl.class);

	public ProductDaoImpl() {
		super();
	}

	/**
	 * returns the basic product class
	 * @return class of basic product
	 */
	@Override
	protected Class<BasicProduct> getClazz() {
		return BasicProduct.class;
	}

	/**
	 * returns a list of BasicProducts ordered by date
	 * @return list of products
	 */
	public List<BasicProduct> getListOrderedByDate() {
		log.info("enter getListOrderedByDate...");
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<BasicProduct> criteria = cb.createQuery(BasicProduct.class);
		Root<BasicProduct> BasicProduct = criteria.from(BasicProduct.class);

		criteria.select(BasicProduct).orderBy(cb.desc(BasicProduct.get("creationDate")));
		return em.createQuery(criteria).getResultList();
	}

}
