package org.generico.persistance;

import java.util.List;

import org.generico.domain.BasicProduct;

/**
 * @name ProductDAO
 * 
 * @description
 * ProductDAO for the Product Domain
 * 
 * @author marcus
 *
 */

public interface ProductDao extends SimpleDao<BasicProduct> {

	/**
	 * get Products ordered by Date
	 * @return
	 */
	public List<BasicProduct> getListOrderedByDate();
	
}
