package org.generico.persistance;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.generico.domain.TestEntity;
import org.springframework.stereotype.Component;

@Component
public class TestDao extends BasicDao<TestEntity> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6378425299549331828L;

	@Override
	protected Class<TestEntity> getClazz() {
		return TestEntity.class;
	}
	
	public List<TestEntity> getAllEntities() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<TestEntity> criteria = cb.createQuery(TestEntity.class);
		Root<TestEntity> user = criteria.from(TestEntity.class);

		criteria.select(user);
		return em.createQuery(criteria).getResultList();
	}
	
}
