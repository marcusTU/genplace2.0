package org.generico.persistance;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.TransactionRequiredException;
import javax.persistence.criteria.CriteriaQuery;

import org.generico.exception.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Offers a basic implementation for accessing the data layer.
 * 
 * @author Stefan Zaufl
 * @version 1
 * @date 27.4.2013
 *
 * @param <T> the class of the entity to be managed
 */
public abstract class BasicDao<T> implements SimpleDao<T> {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = -9077733493454734802L;
	
	@Autowired
	protected EntityManager em;

	public T getEntityById(long id) throws DAOException {
		T entity;
		
		try {
			entity = em.getReference(getClazz(), id);
		} catch(IllegalArgumentException e) {
			throw new DAOException("Can't get Entity!", e);
		} catch(EntityNotFoundException e) {
			entity = null;
		}
		
		return entity;
	}

	@SuppressWarnings("unchecked")
	public List<T> getEntityByQuery(CriteriaQuery<T> query) throws DAOException {
		if(query == null) {
			throw new IllegalArgumentException("query mustn't be null!");
		}
		
		Query q = em.createQuery(query);
		
		List<T> resultList;
		try {
			resultList = q.getResultList();
		} catch (Exception e) {
			throw new DAOException("Can't retieve entitylist!", e);
		}
		
		return resultList;
	}

	@Transactional
	public void deleteEntity(T entity) throws DAOException {
		if(entity == null) {
			throw new IllegalArgumentException("entity mustn't be null!");
		}
		
		try{
			em.remove(entity);
		} catch(IllegalStateException e) {
			throw new DAOException("Can't delete Entity!", e);
		} catch(TransactionRequiredException e) {
			throw new DAOException("Can't delete Entity!", e);
		} 
	}

	@Transactional
	public void deleteEntity(Long id) throws DAOException {
		if(id == null) {
			throw new IllegalArgumentException("id mustn't be null!");
		}
		
		deleteEntity(getEntityById(id));
	}

	@Transactional
	public void updateEntity(T entity) throws DAOException {
		if(entity == null) {
			throw new IllegalArgumentException("entity mustn't be null!");
		}
		
		try{
			em.merge(entity);
		} catch(IllegalStateException e) {
			throw new DAOException("Can't update Entity!", e);
		} catch(TransactionRequiredException e) {
			throw new DAOException("Can't update Entity!", e);
		} catch(EntityExistsException e) {
			throw new DAOException("Can't update Entity!", e);
		}
	}

	@Transactional
	public void createEntity(T entity) throws DAOException {
		if(entity == null) {
			throw new IllegalArgumentException("entity mustn't be null!");
		}
		
		try{
			em.persist(entity);
		} catch(IllegalStateException e) {
			throw new DAOException("Can't update Entity!", e);
		} catch(TransactionRequiredException e) {
			throw new DAOException("Can't update Entity!", e);
		} catch(EntityExistsException e) {
			throw new DAOException("Can't update Entity!", e);
		}
	}
	
	/**
	 * @return the class of the managed Entity, never null
	 */
	protected abstract Class<T> getClazz();

}
