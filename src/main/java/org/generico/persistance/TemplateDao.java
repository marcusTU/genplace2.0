package org.generico.persistance;

import java.util.List;

import org.generico.domain.TagCategory;
import org.generico.domain.Template;

/**
 * @name TemplateDAO
 * 
 * @description
 * DAO for the product templates.
 * 
 * @author Alexander Kiennast
 *
 */
public interface TemplateDao extends SimpleDao<Template> {
	
	/**
	 * Returns a list of tag categories associated with a template.
	 * @return List<TagCategory> - associated tag categories.
	 */
	public List<TagCategory> getAssociatedTagCategories(Template template);
}
