package org.generico.persistance;

import java.io.Serializable;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.generico.exception.DAOException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Interface for simple CRUD-methods in the persistance layer.
 * 
 * @author Stefan Zaufl
 * @version 1
 * @date 27.4.2013
 * @param <T> the Entity to be managed by this DAO
 */

public interface SimpleDao<T> extends Serializable {
	
	/**
	 * Returns the entity with the given id. The method returns <code>null</code> if there is no entity with such an id.
	 * 
	 * @param id the id of the entity
	 * @return the entity with the given id or null
	 * @throws DAOException if there is a problem with the persistence layer
	 */
	T getEntityById(long id) throws DAOException;
	
	/**
	 * Returns all entities that fulfill the given criterias.
	 * 
	 * @param query the criterias for the entities
	 * @return a list, never null, but may be empty
	 * @throws DAOException if there is a problem with the persistence layer
	 * @throws IllegalArgumentException if query is null
	 */
	List<T> getEntityByQuery(CriteriaQuery<T> query) throws DAOException;
	
	
	/**
	 * Removes the given entity from the persistence layer.
	 * @param entity the entity to be removed
	 * @throws DAOException if there is a problem with the persistence layer
	 * @throws IllegalArgumentException if entity is null
	 */
	@Transactional
	void deleteEntity(T entity) throws DAOException;
	
	/**
	 * Removes the entity with the given id from the persistence layer.
	 * @param id the id of the entity to be removed
	 * @throws DAOException if there is a problem with the persistence layer
	 * @throws IllegalArgumentException if id is null
	 */
	@Transactional
	void deleteEntity(Long id) throws DAOException;
	
	/**
	 * Updates the state of the entity in the persistence layer to the state of the given entity.
	 * @param entity the entity with the new data
	 * @throws DAOException if there is a problem with the persistence layer
	 * @throws IllegalArgumentException if entity is null
	 */
	@Transactional
	void updateEntity(T entity) throws DAOException;
	
	/**
	 * Creates the given entity in the persistence layer.
	 * @param entity the entity to be created.
	 * @throws DAOException if there is a problem with the persistence layer
	 * @throws IllegalArgumentException if entity is null
	 */
	@Transactional
	void createEntity(T entity) throws DAOException;
}
