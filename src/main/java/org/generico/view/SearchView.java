package org.generico.view;

import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.generico.domain.BasicProduct;
import org.generico.domain.Tag;
import org.generico.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class SearchView {
	
	@Autowired
	private SearchService searchService;

	// String typed in search field
	@ManagedProperty(value="#{param['q']}")
	private String searchQuery = "";
	
	// String that contains the ids of all tag that we filter for
	@ManagedProperty(value="#{param['t']}")
	private String searchTags = "";
	
	// String that describes how we want to order the results
	@ManagedProperty(value="#{param['o']}")
	private String searchOrder = "";
	
	private List<BasicProduct> products;
	
	
	public SearchView() {
		// void
	}
	
	
	
	public String navigate() {
		return "/search";
	}
	
	public List<Tag> getTags() {
		return new LinkedList<Tag>();
	}
	
	public List<BasicProduct> getProducts() {
		if (products == null) products = searchService.getFilteredProductList(searchQuery, getTags(), searchOrder);
		return products;
	}

	
	public boolean renderSearchResultDescription() {
		return !"".equals(searchQuery);
	}
	

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery.replace('+', ' ').trim();
	}

	public String getSearchTags() {
		return searchTags;
	}

	public void setSearchTags(String searchTags) {
		this.searchTags = searchTags.trim();
	}

	public String getSearchOrder() {
		return searchOrder;
	}

	public void setSearchOrder(String searchOrder) {
		this.searchOrder = searchOrder.trim();
	}

}
