package org.generico.service.messages;

import java.util.Iterator;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * The implementation of the MessageingService-Interface.
 * 
 * @author Stefan Zaufl
 * @see MessageingService
 *
 */
@Component
@Scope(value="request", proxyMode=ScopedProxyMode.INTERFACES)
public class MessageingServiceImpl implements MessageingService {
	private static final Logger logger = LogManager.getLogger(MessageingServiceImpl.class);

	public void info(String localizationId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId(localizationId), null, null);
	}

	public void info(String localizationId, String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId(localizationId), getMessageForId(detailLocalizationId), null);
	}

	public void info(String localizationId, String detailLocalizationId,
			String elementId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId(localizationId), getMessageForId(detailLocalizationId), elementId);
	}

	public void warn(String localizationId) {
		dropMessage(FacesMessage.SEVERITY_WARN, getMessageForId(localizationId), null, null);
	}

	public void warn(String localizationId, String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_WARN, getMessageForId(localizationId), getMessageForId(detailLocalizationId), null);
	}

	public void warn(String localizationId, String detailLocalizationId,
			String elementId) {
		dropMessage(FacesMessage.SEVERITY_WARN, getMessageForId(localizationId), getMessageForId(detailLocalizationId), elementId);
	}

	public void error(String localizationId) {
		dropMessage(FacesMessage.SEVERITY_ERROR, getMessageForId(localizationId), null, null);
	}

	public void error(String localizationId, String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_ERROR, getMessageForId(localizationId), getMessageForId(detailLocalizationId), null);
	}

	public void error(String localizationId, String detailLocalizationId,
			String elementId) {
		dropMessage(FacesMessage.SEVERITY_ERROR, getMessageForId(localizationId), getMessageForId(detailLocalizationId), elementId);
	}

	public void fatal(String localizationId) {
		dropMessage(FacesMessage.SEVERITY_FATAL, getMessageForId(localizationId), null, null);
	}

	public void fatal(String localizationId, String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_FATAL, getMessageForId(localizationId), getMessageForId(detailLocalizationId), null);
	}

	public void fatal(String localizationId, String detailLocalizationId,
			String elementId) {
		dropMessage(FacesMessage.SEVERITY_FATAL, getMessageForId(localizationId), getMessageForId(detailLocalizationId), elementId);
	}

	public void succesfullySaved() {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.saved"), null, null);
	}

	public void succesfullySaved(String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.saved"), getMessageForId(detailLocalizationId), null);
	}

	public void succesfullySaved(String detailLocalizationId, String elementId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.saved"), getMessageForId(detailLocalizationId), elementId);
	}

	public void succesfullyDeleted() {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.deleted"), null, null);
	}

	public void succesfullyDeleted(String detailLocalizationId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.deleted"), getMessageForId(detailLocalizationId), null);
	}

	public void succesfullyDeleted(String detailLocalizationId, String elementId) {
		dropMessage(FacesMessage.SEVERITY_INFO, getMessageForId("standard.successfully.deleted"), getMessageForId(detailLocalizationId), elementId);
	}
	
	/**
	 * Adds a new FacesMessage to the Faces context and ensures, that the rules specified for each severity will be applied.
	 * severity and shortMessage must not be null, all other arguments my be null.
	 * @param servity the level of the message(INFO, WARN, ERROR, FATAL)
	 * @param shortMessage the short summary of the message
	 * @param detailMessage the detailed description of the message
	 * @param elementId the HTML-id of the element the message is for
	 * 
	 * @see FacesMessage.Severity
	 */
	private void dropMessage(FacesMessage.Severity servity, String shortMessage, String detailMessage, String elementId) {
		if(servity == null || shortMessage == null) {
			logger.warn("Can't enqueue message: severity or short message is null(servity=" +
					servity + ", shortMessage=" + shortMessage + ")");
			return;
		}
		
		FacesContext fContext = FacesContext.getCurrentInstance();
		
		Iterator<FacesMessage> mIter = fContext.getMessages();
		boolean isNewError = servity.compareTo(FacesMessage.SEVERITY_ERROR) >= 0;
		
		while(mIter.hasNext()) {
			FacesMessage fm = mIter.next();
			
			if(!isNewError && fm.getSeverity().compareTo(FacesMessage.SEVERITY_ERROR) >= 0) {
				//If the new message is no error message and there is already an error message in the context, abort
				return;
			}
			
			if(isNewError && fm.getSeverity().compareTo(FacesMessage.SEVERITY_ERROR) < 0) {
				//If the new message is an error message, but there is a non-error message in the context, delete it
				mIter.remove();
			}
		}
		
		//Remove double messages
		mIter = fContext.getMessages(elementId);
		
		while(mIter.hasNext()) {
			FacesMessage fm = mIter.next();
			
			if(fm.getSeverity().compareTo(servity) == 0 && fm.getSummary().equals(shortMessage)) {
				//The message already exists, abort
				return;
			}
		}
		
		//Add message to context
		FacesMessage newMessage = new FacesMessage(servity, shortMessage, detailMessage);
		logger.debug("adding new FacesMessage: elementId=" +
				elementId + ", servity=" + servity + ", shortMessage=" + shortMessage + ", detailMessage=" + detailMessage);
		
		fContext.addMessage(elementId, newMessage);
	}
	
	private String getMessageForId(String messageId) {
		if(messageId == null) return null;
		
		Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		ResourceBundle messages = ResourceBundle.getBundle("com.genplace.language", locale);
		
		try {
			return messages.getString(messageId);
		} catch (MissingResourceException ex) {
			logger.warn("Can't load message id='" + messageId + "': no such message for current context!");
			return null;
		}
	}

}
