package org.generico.service.messages;

/**
 * <p>This Service will allow you to push certain common messages to the user like that something has been saved or
 * a message from the localization file. The Service will only allow you to enqueue one message of a kind per request,
 * it will ignore e.g. the second call of successfully saved. Furthermore, if a critical message(error+) is enqueued, old success
 * or information messages will be removed, new ones will be ignored.</p>
 * 
 * @author Stefan Zaufl
 *
 */
public interface MessageingService {
	
	/**
	 * <p>Enqueues an information message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 */
	public void info(String localizationId);
	
	/**
	 * <p>Enqueues an information message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void info(String localizationId, String detailLocalizationId);
	
	/**
	 * <p>Enqueues an information message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message. elemtId is the HTML-id of the element, the message
	 * belongs to.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void info(String localizationId, String detailLocalizationId, String elementId);
	
	
	/**
	 * <p>Enqueues a warning message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 */
	public void warn(String localizationId);
	
	/**
	 * <p>Enqueues a warning message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void warn(String localizationId, String detailLocalizationId);
	
	/**
	 * <p>Enqueues a warning message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message. elemtId is the HTML-id of the element, the message
	 * belongs to.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void warn(String localizationId, String detailLocalizationId, String elementId);
	
	
	/**
	 * <p>Enqueues an error message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * <p>When an error message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 */
	public void error(String localizationId);
	
	/**
	 * <p>Enqueues an error message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * 
	 * <p>When an error message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void error(String localizationId, String detailLocalizationId);
	
	/**
	 * <p>Enqueues an error message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message. elemtId is the HTML-id of the element, the message
	 * belongs to.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * 
	 * <p>When an error message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void error(String localizationId, String detailLocalizationId, String elementId);
	
	
	/**
	 * <p>Enqueues a fatal message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * <p>When a fatal message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 */
	public void fatal(String localizationId);
	
	/**
	 * <p>Enqueues a fatal message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * 
	 * <p>When a fatal message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void fatal(String localizationId, String detailLocalizationId);
	
	/**
	 * <p>Enqueues a fatal message for the user. localizationId is the id of the message in the localization file to be
	 * enqueued. detailLocalizationId is the id for the detailed message. elemtId is the HTML-id of the element, the message
	 * belongs to.</p>
	 * 
	 * <p>The ids must not be null nor empty.</p>
	 * 
	 * <p>When a fatal message is enqueued, all previous generated info- and warn-messages will be removed and new ones
	 * will be ignored.</p>
	 * @param localizationId the id of the message in the localization file
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void fatal(String localizationId, String detailLocalizationId, String elementId);
	
	
	/**
	 * Enqueues a "successfully saved" information message for the user.
	 */
	public void succesfullySaved();
	
	/**
	 * <p>Enqueues a "successfully saved" information message for the user. detailLocalizationId is the id for the detailed
	 * message.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void succesfullySaved(String detailLocalizationId);
	
	/**
	 * <p>Enqueues a "successfully saved" information message for the user. detailLocalizationId is the id for the detailed
	 * message. elemtId is the HTML-id of the element, the message belongs to.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void succesfullySaved(String detailLocalizationId, String elementId);
	
	
	/**
	 * Enqueues a "successfully deleted" information message for the user.
	 */
	public void succesfullyDeleted();
	
	/**
	 * <p>Enqueues a "successfully deleted" information message for the user. detailLocalizationId is the id for the detailed
	 * message.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 */
	public void succesfullyDeleted(String detailLocalizationId);
	
	/**
	 * <p>Enqueues a "successfully deleted" information message for the user. detailLocalizationId is the id for the detailed
	 * message. elemtId is the HTML-id of the element, the message belongs to.</p>
	 * 
	 * <p>The id must not be null nor empty.</p>
	 * 
	 * @param detailLocalizationId the id of the detailed message in the localization file
	 * @param elementId the HTML-id of the element this message belongs to
	 */
	public void succesfullyDeleted(String detailLocalizationId, String elementId);
}
