package org.generico.service;

import java.io.Serializable;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.generico.domain.BasicProduct;
import org.generico.domain.Tag;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("application")
public class SearchService implements Serializable {

	private static final long serialVersionUID = -3364908685570867361L;

	@Autowired
	private EntityManagerFactory emf;
	
	private EntityManager em;
	private FullTextEntityManager fem;
	private QueryBuilder qb;
	
	public SearchService() {
		// void
	}
	
//	@PostConstruct
//	private void initSearch() {
//		try {
//			em = emf.createEntityManager();
//			fem = Search.getFullTextEntityManager(em);
//			fem.createIndexer().startAndWait();
//			qb = fem.getSearchFactory().buildQueryBuilder().forEntity(BasicProduct.class).get();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//			throw new RuntimeErrorException(new Error(e.toString()));
//		}
//	}
	
	
	public List<BasicProduct> getFilteredProductList(String keywords, List<Tag> tags, String searchOrder) {
		String combinedSearchString = keywords;
		for (Tag t : tags) combinedSearchString += " " + t.getName();
		
		System.out.println("Query string for search is: " + combinedSearchString);
		
		try {
			em = emf.createEntityManager();
			fem = Search.getFullTextEntityManager(em);
			fem.createIndexer().startAndWait();
			qb = fem.getSearchFactory().buildQueryBuilder().forEntity(BasicProduct.class).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
			throw new RuntimeErrorException(new Error(e.toString()));
		}
		
		em.getTransaction().begin();
		
		if ("".equals(combinedSearchString.replaceAll(" ", ""))) {
			CriteriaBuilder builder = em.getCriteriaBuilder();
		    CriteriaQuery<BasicProduct> query = builder.createQuery(BasicProduct.class);
		    Root<BasicProduct> root = query.from(BasicProduct.class);
		    query.select(root);

		    return em.createQuery(query).getResultList();
		}
		
		org.apache.lucene.search.Query query = qb
		  .keyword()
		  .onFields("title", "description", "tags.name")
		  .matching(combinedSearchString)
		  .createQuery();

		javax.persistence.Query persistenceQuery = 
		    fem.createFullTextQuery(query, BasicProduct.class);

		// execute search
		List<BasicProduct> result = persistenceQuery.getResultList();

		em.getTransaction().commit();
		em.close();
		
		return result;
	}
	

}
