package org.generico.service;

import java.util.ArrayList;

import org.jboss.logging.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="request")
public class ProductDetailServiceImpl implements ProductDetailService{
	
	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);

	public ArrayList<String> getGalleriaPictures() {
		//FacesContext facesContext=FacesContext.getCurrentInstance();
		//targetFolder = new File(facesContext.getExternalContext().getRealPath("") + "/resources/uploads");
		logger.info("Requested list of galleria pictures.");
		// TODO Load paths from database
		ArrayList<String> example=new ArrayList<String>();
		example.add("bread.jpg");
		example.add("businessman_with_chainsaw.jpg");
		example.add("machine_gun_cat.jpg");
		example.add("say_what_again.jpg");
		return example;
	}
	
}
