package org.generico.service;

import java.util.List;

import org.generico.controller.FileUploadControllerImage;
import org.generico.domain.BasicProduct;
import org.generico.domain.ImagePath;
import org.generico.domain.TagCategory;
import org.generico.domain.Template;
import org.generico.exception.DAOException;
import org.generico.persistance.ProductDao;
import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @name ProductWizardServiceImpl
 * 
 * @description
 * Implementation for the service class of the product wizard.
 * 
 * @author Alexander Kiennast, Marcus Presich
 *
 */

@Component
@Scope("session")
public class ProductServiceImpl implements ProductService{
	
	private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);
	
	private FileUploadControllerImage fuci = new FileUploadControllerImage();
	
	private BasicProduct instance = null;
	
	@Autowired
	private ProductDao productDao;

	
	/**
	 * handles the File Upload of an Product Image
	 * @param event
	 */
	public void handleFileUpload(FileUploadEvent event) {
		ImagePath prodImage = new ImagePath();
		prodImage.setImagePath(fuci.handleFileUpload(event));
		instance.addImagePath(prodImage);
		logger.info("Appended Image "+prodImage.getImagePath());
	}
	
	/**
	 * gives the Product instance back
	 * @return
	 */
	public BasicProduct getInstance() {
		if(instance == null) instance = new BasicProduct();
		
		return instance;
	}
	
	/**
	 * returns all Product Entities by Date
	 * @return
	 */
	public List<BasicProduct> getEntitiesOrderedByDate() {
		return productDao.getListOrderedByDate();
	}
	
	/**
	 * persists a Basic Product instance
	 */
	public void persistProduct() {
		if(instance != null) {
			try {
				logger.info(instance.toString());
				productDao.createEntity(instance);
				instance = new BasicProduct();
			} catch (DAOException e) {
				logger.error("Error while persisting instance!", e);
			}
		}
	}

	public List<TagCategory> getTagCategories(Template template) {
		
		//TODO implement Method!!!!
		return null;
	}

}
