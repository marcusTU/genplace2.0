package org.generico.service;

import java.util.List;

public interface ProductDetailService {
	
	public List<String> getGalleriaPictures();
	
}
