package org.generico.service;

import java.io.Serializable;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.generico.domain.TestEntity;
import org.generico.exception.DAOException;
import org.generico.persistance.TestDao;
import org.generico.service.messages.MessageingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class TestService implements Serializable {
	private static final long serialVersionUID = -1031792507091365925L;
	private static final Logger logger = LogManager.getLogger(TestService.class);

	@Autowired
	private TestDao testDao;
	
	@Autowired
	private MessageingService messageService;
	
	private TestEntity instance;
	
	public TestEntity getInstance() {
		logger.trace("Entering getInstance()");
		if(instance == null) instance = new TestEntity();
		
		return instance;
	}
	
	public List<TestEntity> getEntities() {
		return testDao.getAllEntities();
	}
	
	public void persistInstance() {
		if(instance != null) {
			try {
				testDao.createEntity(instance);
				instance = new TestEntity();
				messageService.succesfullySaved("test.m1", "testForm:testText");
				messageService.succesfullySaved(null, "testForm:testText");
				messageService.succesfullySaved();
				messageService.warn("test.m1", "test.m2");
				messageService.succesfullySaved();
				messageService.succesfullySaved();
			} catch (DAOException e) {
				logger.error("Error while persisting instance!", e);
			}
		}
	}
}
