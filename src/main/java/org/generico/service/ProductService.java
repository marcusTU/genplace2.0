package org.generico.service;

import java.util.List;

import org.generico.domain.TagCategory;
import org.generico.domain.Template;

/**
 * @name ProductWizardService
 * 
 * @description
 * Interface for the service class of the product wizard.
 * 
 * @author Alexander Kiennast, Marcus Presich
 *
 */
public interface ProductService {
	
	public void persistProduct();
	
	public List<TagCategory> getTagCategories(Template template);
}
