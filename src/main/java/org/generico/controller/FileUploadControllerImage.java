package org.generico.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.jboss.logging.Logger;
import org.primefaces.event.FileUploadEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @Name
 * FileUploadController
 *
 * @Description
 * the controller class for the file Upload
 *
 * @author Marcus Presich
 */

@Component
@Scope("session")
public class FileUploadControllerImage implements Serializable {

	/**
	 * UUID
	 */
	private static final long serialVersionUID = 6819216901763624824L;


	private static final Logger log = Logger.getLogger(FileUploadControllerImage.class);

	/**
	 * size
	 */
	private int size = 1024;

	private String fileName;
	

	private File targetFolder;

	/**
	 * handels the file Upload
	 * @param event
	 */
	public String handleFileUpload(FileUploadEvent event) {  
		log.info("Method handleFileUpload invoked");

		InputStream inputStream = null;
		OutputStream out = null;

		try {
			if(targetFolder == null) {
				FacesContext facesContext=FacesContext.getCurrentInstance();
				targetFolder = new File(facesContext.getExternalContext().getRealPath("") + "/resources/uploads");

				targetFolder.mkdirs();
				log.info("Target Folder of Picture: " + targetFolder);
			}

			fileName = event.getFile().getFileName();
			inputStream = event.getFile().getInputstream();
			File outFile = new File(targetFolder, event.getFile().getFileName());
			log.info("copy file stream to " + outFile.getAbsolutePath());
			out = new FileOutputStream(outFile);
			int read = 0;
			byte[] bytes = new byte[size];
			log.info("read file stream");			
			while ((read = inputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}

			out.flush();

			FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");  
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (IOException e) {
			log.error(e);
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error while uploading Image!", "'"+fileName+"'");  
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} finally {
			try {
				if(inputStream!=null) {
					inputStream.close();
				}
			} catch(IOException e) {
			}
			try {
				if(out!=null) {
					out.close();
				}
			} catch(IOException e) {

			}
		}
		return "resources/uploads/" + fileName;
	}
	
}
